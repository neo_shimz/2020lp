# 2020LP
## Preprosを使用して下さい。
## 必ずsrc以下を編集して下さい。
### ソース以下のファイルでjp、en、assetsを上書きします！
.
├── README.md  
├── assets  
│   ├── css  
│   │   ├── common  
│   │   │   ├── article.css  
│   │   │   ├── common_pc.css  
│   │   │   ├── common_sp.css  
│   │   │   ├── common_specialheader.css  
│   │   │   └── reset.css  
│   │   ├── form.css  
│   │   ├── lightcase  
│   │   │   ├── lightcase.css  
│   │   │   ├── lightcase.min.css  
│   │   │   └── lightcase_fonts  
│   │   │       ├── lightcase.eot  
│   │   │       ├── lightcase.svg  
│   │   │       ├── lightcase.ttf  
│   │   │       └── lightcase.woff  
│   │   ├── pages  
│   │   │   └── special  
│   │   │       └── makethebeat  
│   │   │           ├── top.css  
│   │   │           └── top.css.map  
│   │   ├── terms.css  
│   │   ├── terms.css.map  
│   │   ├── top.css  
│   │   └── top.css.map  
│   ├── img  
│   │   ├── available_sns.svg  
│   │   ├── common  
│   │   │   └── text_tokyo2020.png  
│   │   └── winding_border_sp.svg  
│   ├── js  
│   │   ├── common  
│   │   │   ├── lib  
│   │   │   │   ├── jquery.cookie.js  
│   │   │   │   └── libs.js  
│   │   │   └── script.js  
│   │   ├── form.js  
│   │   ├── lightcase.js  
│   │   ├── lightcase.min.js  
│   │   ├── modal  
│   │   │   └── jquery.magnific-popup.min.js  
│   │   ├── script.js  
│   │   └── script.js.map  
│   ├── m  
│   │   ├── img  
│   │   │   ├── common  
│   │   │   │   ├── button_menu_close_sp.png  
│   │   │   │   ├── button_menu_sp.png  
│   │   │   │   ├── icon_header_bar.png  
│   │   │   │   └── logo_tokyo2020.png  
│   │   │   └── sprites  
│   │   │       └── sprite.png  
│   │   ├── js  
│   │   │   └── common  
│   │   │       ├── lib  
│   │   │       │   └── libs.js  
│   │   │       └── utils  
│   │   │           └── utils.js  
│   │   └── sprites  
│   │       └── sprite.png  
│   └── sprites  
│       └── sprite.png  
├── en  
│   ├── form.html  
│   ├── index.html  
│   └── termes  
│       └── index.html  
├── index.html  
├── jp  
│   ├── form.html  
│   ├── index.html  
│   └── termes  
│       └── index.html  
├── prepros-6.config  
└── src  
    ├── assets  
    │   ├── js  
    │   │   ├── form.js  
    │   │   └── script.js  
    │   └── scss  
    │       ├── form.scss  
    │       ├── terms.scss  
    │       └── top.scss  
    ├── en  
    │   ├── footer.pug  
    │   ├── form.pug  
    │   ├── header.pug  
    │   ├── index.pug  
    │   └── terms.pug  
    ├── index.pug  
    ├── jp  
    │   ├── footer.pug  
    │   ├── form.pug  
    │   ├── header.pug  
    │   ├── index.pug  
    │   └── terms.pug  
    ├── kraisel  
    │   ├── kraisel_pc.css  
    │   ├── kraisel_pc.scss  
    │   ├── kraisel_sp.css  
    │   └── kraisel_sp.scss  
    └── partials  
        ├── common_meta.pug  
        ├── common_script.pug  
        ├── fb-root.pug  
        ├── gtm.pug  
        ├── noscript.pug  
        └── sp_navi.pug  
