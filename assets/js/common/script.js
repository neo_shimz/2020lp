var BREAK_POINT = 768;
$(window).on('pageshow', function(event) {
  //ブラウザの戻るボタン押下時
  setTimeout(function(){
    $("#globalNavigationInner li a").blur();
  }, 200);
});
//ウインドウリサイズで
$(window).on('resize', function () {
    b = $(window);
    bw = b.innerWidth();
    bh = b.innerHeight();
}).trigger('resize');
$(window).on('hashchange', function () {
  var $dir = location.href.replace(/\?.*/g,'').split("/");
  var $dir_name = $dir[4];
  var $dir_name2 = $dir[5];
  var dom_name = ".sidebar-"+$dir_name;
  var dom_name_redhot = ".list_"+$dir[$dir.length -2];
  //console.log('hashchange', $dir, $dir_name, $dir_name2, dom_name, dom_name_redhot);
  if($dir_name == "accessibility"){
    $(".list_accessibility").addClass("redhot");
  }else if($dir_name2 == "release"){
    $(".list_press-room-release").addClass("redhot");
  }
}).trigger('hashchange');
/*
 * helper functions
 */
;( function( window ) {
  var $dir = location.href.replace(/\?.*/g,'').split("/");
  var $dir_name = $dir[4];
  /*
   *globalnav の表示制御
   */
  if($dir_name == "" || $dir_name.match(/index\.htm/)){
    var home_category_name = $('#homeBtn > a').text();
    $('#homeBtn > a').attr({'aria-current': 'page'}).empty().html('<em>' + home_category_name + '</em>');
  }else if($dir_name == "news"){
    $("#newsBtn").addClass("currentGlobal");
  } else if($dir_name == "games"){
    $("#gamesBtn").addClass("currentGlobal");
  } else if($dir_name == "get-involved"){
    $("#get-involvedBtn").addClass("currentGlobal");
  } else if($dir_name == "organising-committee"){
    $("#organising-committeeBtn").addClass("currentGlobal");
  }
  var $gnav = $('#globalNavigationInner > li');
  $('#globalNavigationInner > li.currentGlobal > a').attr({'aria-current': 'page'}).each(function(index, element){
    var category_name = $(element).text();
    $(element).empty().html('<em>' + category_name + '</em>');
  });
  $('#globalNavigationInner li .megaMenu li > a').on('focus', function(){
    $(this).parent().parent().parent().parent().addClass("hover");
  });
  $('#globalNavigationInner li > a.parent').on('focus', function(){
    $(this).parent().addClass("hover");
    var self = $(this).parent();
    $gnav.not(self).removeClass("hover").find('div.megaMenu').attr({'aria-hidden': true});
    $(this).siblings('div.megaMenu').attr({'aria-hidden': false});
  });
  $('a').not($('#globalNavigationInner li a')).on('focus', function(){
    $gnav.removeClass("hover").find('div.megaMenu').attr({'aria-hidden': true});
  });
  $gnav.on('mouseout', function(){
    $gnav.removeClass("hover").find('div.megaMenu').attr({'aria-hidden': true});
  });
  // オンマウスしている箇所以外は表示しない
  $gnav.on({
      'mouseenter':function(){
        $(this).find('.megaMenu').removeClass('hidenav').end().siblings().find('.megaMenu').addClass('hidenav');
        $(this).find('.megaMenu').attr({'aria-hidden': false});
      },
      'mouseleave':function(){
        $gnav.find('.megaMenu').removeClass('hidenav');
        $(this).find('.megaMenu').attr({'aria-hidden': true});
      }
  });
  /*
   *sidebar の表示制御
   */
  //news sidebar 表示特別制御
  if($dir[4]  =="news"){
    var btn = $("#sidebar").find(".boxBtnPrimary");
    var btn2 = $("#sidebar").find(".boxSearch");
    btn.css({ "display":"block"});
    btn2.css({ "display":"block"});
  }
  //第一階層 sidebar 表示制御
  if($dir.length === 6 ){
    var dom_name = ".sidebar-"+$dir_name;
    var dom_name_redhot = ".list_"+$dir[$dir.length -2];
    dom_name = $("#sidebar").find(dom_name);
    dom_name_redhot = $("#sidebar").find(dom_name_redhot);
    dom_name.addClass("displayBlock");
    var current_text = dom_name_redhot.addClass("redhot").text();
    dom_name_redhot.find('a').attr({'aria-current':'page'}).html('<em>' + current_text + '</em>');
  //第二階層 sidebar 表示制御
  }else if($dir.length >= 7){
    var $dir_name_second = $dir[5];
    var dom_name = ".sidebar-"+$dir_name;
    dom_name_second = ".sidebar-"+$dir_name+"-"+$dir_name_second;
    var dom_name_redhot = ".list_"+$dir_name;
    if(dom_name ==".sidebar-news"){
      for (var i=0; i<1; i++) {
        if(i <= 3){
          //半角英数字以外
          if(!$dir[$dir.length-($dir.length-6-i+1)].match(/^\d+$/) ){
            dom_name_redhot+="-"+$dir[$dir.length-($dir.length-6-i+1)];
          }
        }
      }
    }else{
      for (var i=0; i<$dir.length-6; i++) {
        if(i <= 1){
          dom_name_redhot+="-"+$dir[$dir.length-($dir.length-6-i+1)];
        }
      }
    }
    dom_name_redhot = $("#sidebar").find(dom_name_redhot);
    var current_text = dom_name_redhot.addClass("redhot").children('a').text();
    dom_name_redhot.children('a').attr({'aria-current':'page'}).html('<em>' + current_text + '</em>');
    dom_name = $("#sidebar").find(dom_name);
    dom_name_second = $("#sidebar").find(dom_name_second);
    dom_name.addClass("displayBlock");
    if(dom_name_second.length){
      dom_name_second.addClass("displayBlock");
    }
  //テンプレ用全アクティブ化
  }else{
    $("#sidebar .iconListSecondary").css({"display":"block"});
    $("#sidebar .iconListTertiary").css({"display":"block"});
  }
  //モバイル端末判別関数
  function deviceCheck() {
    var check = {
      ios:device.ios(),
      mobile:device.mobile(),
      tablet:device.tablet(),
      ipad:device.ipad(),
      iphone:device.iphone(),
      ipod:device.ipod(),
      android:device.android(),
      androidTablet:device.androidTablet(),
      windows:device.windows(),
      windowsPhone:device.windowsPhone(),
      windowsTablet:device.windowsTablet()
    };
    return check;
  }
  window.deviceCheck = deviceCheck();
function visibilityCheck(){
    var hiddenCheck = {};
    var hidden, visibilityChange;
    if (typeof document.hidden !== "undefined") { // Opera 12.10 や Firefox 18 以降でサポート
      hidden = "hidden";
      visibilityChange = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
      hidden = "mozHidden";
      visibilityChange = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
      hidden = "msHidden";
      visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
      hidden = "webkitHidden";
      visibilityChange = "webkitvisibilitychange";
    }
    hiddenCheck.hidden = hidden;
    hiddenCheck.visibilityChange = visibilityChange;
    return hiddenCheck;
};
  window.visibilityCheck = visibilityCheck();
  /*
   * アンカー アニメーション
   */
  $('.scrollLink').click(function() {
      var speed = 400;
      var href= $(this).attr('href');
      var target = $(href == '#' || href == '' ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
  });
})( window );
/*
 * ウインドウロード後のinitFn
 */
$(document).ready(function(){
  var window_width = $(window).width();
  function initFn() {
    if($(window).width() >= BREAK_POINT){
      $('.autoHeight').tile(2);
    }
  }
  /**
   * PC/SP出し分け要素
   */
  function switchElement () {
    if($(window).width() >= BREAK_POINT){
      // PCモード
      $('.sp-element').attr('aria-hidden', true);
      $('.pc-element').removeAttr('aria-hidden');
      $('.sp-element').each(function(index, el){
        if($(el).css('display') !== 'none'){
          $('.sp-element').removeAttr('aria-hidden');
        }
      });
      // スライダーにイベント通知
      $('.slick-slider').trigger('startPcMode');
    } else {
      // SPモード
      $('.pc-element').attr('aria-hidden', true);
      $('.sp-element').removeAttr('aria-hidden');
      // スライダーにイベント通知
      $('.slick-slider').trigger('endPcMode');
    }
  }
  jQuery.event.add(window, 'load', function() {
    initFn();
    switchElement();
  });
  $(window).on('resize', function(){
    if(window_width < BREAK_POINT && $(window).width() >= BREAK_POINT){
      // PCモードへの変更時
      initFn();
      switchElement();
    }else if(window_width >= BREAK_POINT && $(window).width() < BREAK_POINT) {
      //SPモードへの変更
      $('.autoHeight').removeAttr('style');
      switchElement();
    }
    window_width = $(window).width();
  });
  // 1秒ごとにグロナビの高さを揃える
  setInterval(function(){
      $('#globalNavigationInner > li > a > *').tile(5);
  },1000);
});
if(!$('html').hasClass('pc-only')){
  // レスポンシブCSSが当たるためのクラス属性
  $('html').addClass('responsive');
}
/**
 * 画像保護
 */
function protectImage () {
  $('.image-protect img').on('contextmenu mousedown mousemove', function(e){
  //$('.image-protect img').on('contextmenu', function(e){
    // 右クリック禁止
    e.preventDefault();
  });
  /*
    var timer;
    $(".image-protect img").on("touchstart",function(){
        timer = setTimeout(function(){
            alert("画像は保存できません")
        },500)
        return false;
    });
    $(".image-protect img").on("touchend",function(){
        clearTimeout(timer);
        return false;
    });
*/
/*
    PagePlugin = function(op) {
            this;
    };
    PagePlugin.prototype = {
        init: function(){
                var self;
                self = this;
        },
        chancelEvent:function(event){
                var self;
                self = this;
                event.preventDefault();
        }
    }
    document.onselectstart = function( e ){
        var event = e || window.event;
        var tagName = '';
        try{
            tagName = (event.target || event.srcElement).tagName.toLowerCase();
        }
        catch(e){}
        if( tagName != 'textarea' && tagName != 'input'){
            return false;
        }
    }
            var main;
            main = new PagePlugin();
            $(window).on('load',this,function(){
                    main.init();
            });
            $(document).on('mousedown','img',function(event){
                    main.chancelEvent(event);
            });
            $(document).on('selectstart','img',function(event){
                    main.chancelEvent(event);
            });
*/
}
if($('.image-protect').length > 0){
  protectImage();
}
/**
 * ヘッダー サイト内検索フォーム開閉
 */
;( function( window ) {
  function searchBoxHandler () {
    var $this = $('.js-headerSearch'),
      $btn = $this.find('.headerSearchTrigger'),
      $target = $this.find('#headerSearchBlock'),
      $inputText = $target.find('#headerSearchBlockText'),
      $btnClose = $target.find('.headerSearchClose');

    // 検索フォーム裏の要素
    var $languageBox = $('#languageBox'),
      $toPressBox = $('#toPressBox');

    $btn.on('click', function () {
      $target.attr('aria-hidden', 'false');
      $btn
        .blur()
        .attr({
          'aria-hidden': 'true',
          'tabindex': '-1'
        });
      $inputText.focus();
      $languageBox.css('display', 'none');
      $toPressBox.css('display', 'none');
    });

    $btnClose.on('click', function () {
      $target.attr('aria-hidden', 'true');
      $btnClose.blur();
      $btn
        .attr('aria-hidden', 'false')
        .removeAttr('tabindex')
        .focus();
      $languageBox.removeAttr('style');
      $toPressBox.removeAttr('style');
    });
  }

  window.searchBoxHandler = searchBoxHandler();
})( window );