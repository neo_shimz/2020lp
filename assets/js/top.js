$(function(){
  $('.slider').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    fade: true,
    speed: 500,
    dots: false,
    cssEase: 'linear',
    arrows: false,
  });

  $('.slider-prev').on('click', function() {
    $('.slider').slick('slickNext');
  });

  $('.slider-next').on('click', function() {
    $('.slider').slick('slickNext');
  });

  var pauseFalg = true;
  $(".slider-pause").on("click", function(){
    if( pauseFalg ){
      $('.slider').slick("slickPause");
      $(".slider-pause").attr("class", "slider-pause play");
      pauseFalg = false;
    }else{
      $('.slider').slick("slickPlay");
      $(".slider-pause").attr("class", "slider-pause stop");
      pauseFalg = true;
    }

  })


})
