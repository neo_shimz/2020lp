!function(modules) {
    function __webpack_require__(moduleId) {
        if (installedModules[moduleId]) return installedModules[moduleId].exports;
        var module = installedModules[moduleId] = {
            exports: {},
            id: moduleId,
            loaded: !1
        };
        return modules[moduleId].call(module.exports, module, module.exports, __webpack_require__),
        module.loaded = !0, module.exports;
    }
    var installedModules = {};
    return __webpack_require__.m = modules, __webpack_require__.c = installedModules,
    __webpack_require__.p = "", __webpack_require__(0);
}([ function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), PageTop = __webpack_require__(3), SearchForm = __webpack_require__(6), GlobalNavigation = __webpack_require__(7), AccordionMaker = __webpack_require__(9), PullDownMaker = __webpack_require__(11), TabFocus = __webpack_require__(13), PageAnchor = __webpack_require__(14), SlideSetter = __webpack_require__(15);
    window.requestAnimFrame = function() {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(callback) {
            return window.setTimeout(callback, 1e3 / 60);
        };
    }(), window.cancelAnimFrame = function() {
        return window.cancelAnimationFrame || window.cancelRequestAnimationFrame || window.webkitCancelAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelAnimationFrame || window.mozCancelRequestAnimationFrame || window.msCancelAnimationFrame || window.msCancelRequestAnimationFrame || window.oCancelAnimationFrame || window.oCancelRequestAnimationFrame || function(id) {
            window.clearTimeout(id);
        };
    }(), $(document).ready(function() {
        new Main();
    });
    var Main = function(_super) {
        function Main() {
            _super.call(this), FastClick.attach(document.body), new PageTop(), new SearchForm(),
            new GlobalNavigation(), new PageAnchor(), new AccordionMaker(), new PullDownMaker(),
            new TabFocus(), new SlideSetter();
        }
        return __extends(Main, _super), Main;
    }(EventDispatcher);
    module.exports = Main;
}, function(module, exports, __webpack_require__) {
    var Event = __webpack_require__(2), EventDispatcher = function() {
        function EventDispatcher(_target) {
            var _this = this;
            void 0 === _target && (_target = null), this._target = _target, this._listeners = {},
            this.addEventListener = function(types, listener, useCapture) {
                if (void 0 === useCapture && (useCapture = !1), _this._target) return void _this._target.addEventListener(types, listener, useCapture);
                for (var typeList = types.split(/\s+/), i = 0, l = typeList.length; l > i; i++) _this._listeners[typeList[i]] = listener;
            }, this.removeEventListener = function(types, listener) {
                if (_this._target) return void _this._target.removeEventListener(types, listener);
                for (var type, typeList = types.split(/\s+/), i = 0, l = typeList.length; l > i; i++) type = typeList[i],
                (null == listener || _this._listeners[type] === listener) && delete _this._listeners[type];
            }, this.dispatchEvent = function(type, data, context) {
                if (void 0 === data && (data = {}), void 0 === context && (context = _this), _this._target) {
                    if (window.CustomEvent) var event = new CustomEvent(type, data); else {
                        var event = document.createEvent("CustomEvent");
                        event.initCustomEvent(type, !0, !0, data);
                    }
                    return void _this._target.dispatchEvent(event);
                }
                var listener;
                if (listener = _this._listeners[type]) {
                    var e = new Event(type);
                    e.data = data, listener.call(context, e);
                }
                return !0;
            }, this.clearEventListener = function() {
                _this._listeners = {};
            };
        }
        return EventDispatcher;
    }();
    module.exports = EventDispatcher;
}, function(module, exports) {
    var Event = function() {
        function Event(type) {
            this.type = type, this.data = {}, this.defaultPrevented = !1, this.timeStamp = Date.now();
        }
        return Event.prototype.preventDefault = function() {
            this.defaultPrevented = !0;
        }, Event.COMPLETE = "complete", Event.INIT = "init", Event.CHANGE = "change", Event.RESIZE = "resize",
        Event.SCROLL = "scroll", Event.REMOVED = "removed", Event.SUCCESS = "success", Event.ERROR = "error",
        Event.IO_ERROR = "error", Event;
    }();
    module.exports = Event;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Ease = __webpack_require__(4), MouseEvent = __webpack_require__(5), PageTop = function(_super) {
        function PageTop() {
            // ページトップのアンカーリンクを設定する
            $('#contents_contaienr').prepend('<a id="contents_top_anchor"tabindex="-1">ページトップ</a>');
            // ページトップ スクロールアニメーション関数
            function scrollTopAnimation () {
              $("#contents_contaienr").velocity("stop").velocity("scroll", {
                  duration: 1e3,
                  delay: 0,
                  easing: Ease.EaseOutSine,
                  complete: function () {
                    $('#contents_top_anchor').focus();
                  }
              });
            }
            _super.call(this), this.onPageTop = function() {
              scrollTopAnimation();
            }, this._node = $("#btn_pagetop a.ui"), $(window).bind("TO_PAGE_TOP", this.onPageTop),
            this._node.bind(MouseEvent.MOUSE_DOWN, this.onPageTop);
            this._node.bind("keydown", function (e) {
              if (e.keyCode === 13) scrollTopAnimation();
            });
        }
        return __extends(PageTop, _super), PageTop;
    }(EventDispatcher);
    module.exports = PageTop;
}, function(module, exports) {
    var Ease;
    !function(Ease_1) {
        Ease_1.Linear = "linear", Ease_1.Ease = "ease", Ease_1.Ease_IN = "ease-in", Ease_1.Ease_OUT = "ease-out",
        Ease_1.Ease_IN_OUT = "ease-in-out", Ease_1.EaseInSine = "easeInSine", Ease_1.EaseOutSine = "easeOutSine",
        Ease_1.EaseInOutSine = "easeInOutSine", Ease_1.EaseInQuad = "easeInQuad", Ease_1.EaseOutQuad = "easeOutQuad",
        Ease_1.EaseInOutQuad = "easeInOutQuad", Ease_1.EaseInCubic = "easeInCubic", Ease_1.EaseOutCubic = "easeOutCubic",
        Ease_1.EaseInOutCubic = "easeInOutCubic", Ease_1.EaseInQuart = "easeInQuart", Ease_1.EaseOutQuart = "easeOutQuart",
        Ease_1.EaseInOutQuart = "easeInOutQuart", Ease_1.EaseInQuint = "easeInQuint", Ease_1.EaseOutQuint = "easeOutQuint",
        Ease_1.EaseInOutQuint = "easeInOutQuint", Ease_1.EaseInExpo = "easeInExpo", Ease_1.EaseOutExpo = "easeOutExpo",
        Ease_1.EaseInOutExpo = "easeInOutExpo", Ease_1.EaseInCirc = "easeInCirc", Ease_1.EaseOutCirc = "easeOutCirc",
        Ease_1.EaseInOutCirc = "easeInOutCirc";
    }(Ease || (Ease = {})), module.exports = Ease;
}, function(module, exports) {
    var MouseEvent = function() {
        function MouseEvent() {}
        return Object.defineProperty(MouseEvent, "CLICK", {
            get: function() {
                return "click";
            },
            enumerable: !0,
            configurable: !0
        }), Object.defineProperty(MouseEvent, "MOUSE_DOWN", {
            get: function() {
                return "ontouchstart" in window ? "touchstart" : "mousedown";
            },
            enumerable: !0,
            configurable: !0
        }), Object.defineProperty(MouseEvent, "MOUSE_UP", {
            get: function() {
                return "ontouchend" in window ? "touchend" : "mouseup";
            },
            enumerable: !0,
            configurable: !0
        }), Object.defineProperty(MouseEvent, "MOUSE_MOVE", {
            get: function() {
                return "ontouchmove" in window ? "touchmove" : "mousemove";
            },
            enumerable: !0,
            configurable: !0
        }), Object.defineProperty(MouseEvent, "MOUSE_OVER", {
            get: function() {
                return "mouseenter";
            },
            enumerable: !0,
            configurable: !0
        }), Object.defineProperty(MouseEvent, "MOUSE_OUT", {
            get: function() {
                return "mouseleave";
            },
            enumerable: !0,
            configurable: !0
        }), MouseEvent;
    }();
    module.exports = MouseEvent;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Ease = __webpack_require__(4), MouseEvent = __webpack_require__(5), SearchForm = function(_super) {
        function SearchForm() {
            var _this = this;
            _super.call(this), this._isOpen = !1, this.onGNShow = function() {
                // _this._trigger.hide();
            }, this.onGNHide = function() {
                _this._trigger.show();
            }, this.onBlurHD = function() {
                _this._isOpen && (_this._isOpen = !1, _this._isOpen ? _this.show() : _this.hide());
            }, this.onFocusHD = function() {
                $(window).trigger("Search_Focus"), _this._isOpen || (_this._isOpen = !0, _this._isOpen ? _this.show() : _this.hide());
            }, this.onGlobalMenuStateChange = function() {
                _this._isOpen = !1, _this.hide();
            }, this.toggleHD = function() {
                _this._isOpen = !_this._isOpen, _this._isOpen ? _this.show() : _this.hide();
            }, this.show = function() {
                _this._trigger.addClass("active"), _this._view.velocity("stop").velocity({
                    height: _this._inner.outerHeight()
                }, {
                    duration: 200,
                    delay: 0,
                    easing: Ease.EaseOutSine
                });
            }, this.hide = function() {
                _this._trigger.removeClass("active"), _this._view.velocity("stop").velocity({
                    height: 0
                }, {
                    duration: 200,
                    delay: 0,
                    easing: Ease.EaseOutSine
                });
            }, this._trigger = $("#hd_btn_search"), this._view = $("#search_form"), this._inner = this._view.find(".view"),
            this._inputForm = this._view.find('input[type="text"]'), this._inputForm.focus(this.onFocusHD),
            this._inputTrigger = this._view.find('input[type="submit"]'), this._view.focusin(this.onFocusHD),
            this._view.focusout(this.onBlurHD), $(window).bind("GLOBAL_MENU_STATE_CHANGE", this.onGlobalMenuStateChange),
            $(window).bind("GN_SHOW", this.onGNShow), $(window).bind("GN_HIDE", this.onGNHide),
            this._trigger.bind(MouseEvent.MOUSE_DOWN, this.toggleHD);
        }
        return __extends(SearchForm, _super), SearchForm;
    }(EventDispatcher);
    module.exports = SearchForm;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Ease = __webpack_require__(4), GlobalMenuAccordion = __webpack_require__(8), GlobalNavigation = function(_super) {
        function GlobalNavigation() {
            var _this = this;
            _super.call(this), this._hideElements = [], this._isOpen = !1, this._isFocus = !1,
            this.onResizeHD = function() {
                _this._isOpen && $.Velocity.hook(_this._wrapper, "translateX", -_this._view.outerWidth() + "px");
            }, this.setUpAccordion = function() {
                _this._globalMenuList.find("li").each(function() {
                    $(this).hasClass("accordion") && new GlobalMenuAccordion($(this));
                });
            }, this.clickHD = function(e) {
                if (0 !== $(e.currentTarget).find(">a").length) {
                    var _target = $(e.currentTarget).find(">a");
                    // "_blank" === _target.attr("target") ? window.open(_target.attr("href")) : location.href = _target.attr("href");
                }
            }, this.onFocusHD = function() {
                _this._isFocus = !0, _this._isOpen || (_this._isOpen = !0, _this._isOpen ? _this.show() : _this.hide());
            }, this.onFocusOutHD = function() {
                var _self = _this;
                clearTimeout(_this._focusChk), _this._isFocus = !1, _this._focusChk = setTimeout(function() {
                    if (!_self._isFocus) {
                        if (!_self._isOpen) return;
                        _self._isOpen = !1, _self._isOpen ? _self.show() : _self.hide();
                    }
                }, 100);
            }, this.show = function() {
                $(window).trigger("GN_SHOW"), _this._wrapper.velocity("stop").velocity({
                    translateX: -_this._view.outerWidth()
                }, {
                    duration: 300,
                    delay: 0,
                    easing: "linear",
                    complete: _this.heightFix,
                    progress: _this.reflow
                }), _this._trigger.addClass("active");
                _this._node.attr({'aria-hidden': false});
                var i = 0;
                for (i = 0; i < _this._hideElements.length; i++) _this._hideElements[i].velocity({
                    translateX: -_this._view.outerWidth()
                }, {
                    duration: 300,
                    delay: 0,
                    easing: "linear"
                });
            }, this.hide = function() {
                $(window).trigger("GN_HIDE"), _this._wrapper.velocity("stop").velocity({
                    translateX: 0
                }, {
                    duration: 300,
                    delay: 0,
                    easing: Ease.EaseOutSine
                }), _this._trigger.removeClass("active");
                _this._node.attr({'aria-hidden': true});
                var i = 0;
                for (i = 0; i < _this._hideElements.length; i++) _this._hideElements[i].velocity({
                    translateX: 0
                }, {
                    duration: 300,
                    delay: 0,
                    easing: Ease.EaseOutSine
                });
                _this._view.removeAttr("style"), $("html, body").scrollTop(0);
            }, this.hideComplete = function() {
                _this._node.hide(), _this._view.removeAttr("style"), $("html, body").scrollTop(0);
            }, this.heightFix = function() {
                _this._view.css({
                    height: _this._node.outerHeight() + 65
                });
            }, this.reflow = function() {
                var _node = document.getElementById("contents_wrapper");
                _node.style.display = "none", _node.offsetHeight, _node.style.display = "block",
                _node = void 0;
            }, this.toggleMenu = function() {
                $(window).trigger("GLOBAL_MENU_STATE_CHANGE"), _this._isOpen = !_this._isOpen, _this._isOpen ? _this.show() : _this.hide();
            }, this.onGnStateChange = function() {
                _this._isOpen && _this._view.css({
                    height: _this._node.outerHeight() + 65
                });
            }, this._view = $("#contents_contaienr"), this._trigger = $("#hd_btn_menu"), this._wrapper = $("#contents_wrapper"),
            this._node = $("#global_menu"), this._hideElements.push($("#site_logo")), this._globalMenuList = $("#global_menulist"),
            this.setUpAccordion(), this._trigger.bind("click", this.toggleMenu), $(window).bind("GLOBAL_MENU_PROGRESS", this.onGnStateChange);
            var _self = this;
            this._node.find("li").each(function() {
                $(this).bind("click", _self.clickHD);
            }), this._node.find(".trigger").each(function() {
                $(this).bind("click", _self.clickHD);
            }), this._node.focusin(this.onFocusHD), this._node.focusout(this.onFocusOutHD),
            $(window).bind("Search_Focus", this.onFocusOutHD), $(window).bind("resize", this.onResizeHD);
        }
        return __extends(GlobalNavigation, _super), GlobalNavigation;
    }(EventDispatcher);
    module.exports = GlobalNavigation;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Ease = __webpack_require__(4), GlobalMenuAccordion = function(_super) {
        function GlobalMenuAccordion(_node) {
            var _this = this;
            _super.call(this), this._node = _node, this._isOpen = !1, this.blur = function() {
                _this._isOpen && (_this._isOpen = !1, _this.hide());
            }, this.toggleHD = function() {
                _this._isOpen = !_this._isOpen, _this._isOpen ? _this.show() : _this.hide();
            }, this.show = function() {
                _this._isOpen = !0, _this._trigger.addClass("active"), _this._view.velocity("stop").velocity({
                    height: _this._list.outerHeight()
                }, {
                    duration: 500,
                    delay: 0,
                    easing: Ease.EaseOutSine,
                    progress: _this.onProgress
                });
            }, this.hide = function() {
                _this._trigger.removeClass("active"), _this._view.velocity("stop").velocity({
                    height: 0
                }, {
                    duration: 500,
                    delay: 0,
                    easing: Ease.EaseOutSine,
                    progress: _this.onProgress
                });
            }, this.onProgress = function() {
                $(window).trigger("GLOBAL_MENU_PROGRESS");
            }, this._view = this._node.find(">.view"), this._trigger = this._node.find(">.trigger"),
            this._list = this._view.find(">ul"), this._trigger.bind("click", this.toggleHD),
            this._node.focusin(this.show), this._list.find("li").eq(this._list.find("li").length - 1).focusout(this.blur),
            $(window).bind("GN_HIDE", this.blur);
        }
        return __extends(GlobalMenuAccordion, _super), GlobalMenuAccordion;
    }(EventDispatcher);
    module.exports = GlobalMenuAccordion;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Accordion = __webpack_require__(10), AccordionMaker = function(_super) {
        function AccordionMaker() {
            _super.call(this), $(".accordion").each(function() {
                new Accordion($(this));
            });
        }
        return __extends(AccordionMaker, _super), AccordionMaker;
    }(EventDispatcher);
    module.exports = AccordionMaker;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Ease = __webpack_require__(4), Accordion = function(_super) {
        function Accordion(_target) {
            var _this = this;
            _super.call(this), this._target = _target, this._isOpen = !0, this.jadgeDefault = function() {
                _this._isOpen = "open" === _this._node.attr("data-default"), _this._isOpen || (_this._trigger.addClass("active"),
                _this._view.velocity("stop").velocity({
                    height: 0
                }, {
                    duration: 0,
                    delay: 0,
                    easing: Ease.EaseInOutCubic
                }));
            }, this.toggleHD = function() {
                _this._isOpen = !_this._isOpen, _this._isOpen ? _this.show() : _this.hide();
            }, this.show = function() {
                _this._trigger.removeClass("active"), _this._view.velocity("stop").velocity({
                    height: _this._inner.outerHeight()
                }, {
                    duration: 500,
                    delay: 0,
                    easing: Ease.EaseInOutCubic,
                    complete: function(){
                        _this._isOpen = true;
                        _this._node.parent('.accordionText').addClass('open');
                        _this._node.addClass('accordion-open').attr({'aria-expanded': true});
                        _this._view.removeAttr('style').addClass('accordion-open');
                    }
                });
            }, this.hide = function() {
                _this._trigger.addClass("active"), _this._view.velocity("stop").velocity({
                    height: 0
                }, {
                    duration: 500,
                    delay: 0,
                    easing: Ease.EaseInOutCubic,
                    complete: function(){
                        _this._node.parent('.accordionText').removeClass('open');
                        _this._node.removeClass('accordion-open').attr({'aria-expanded': false});
                        _this._view.removeClass('accordion-open');
                    }
                });
            }, this.openHD = function() {
                _this._isOpen || (_this._isOpen = !0, _this.show());
            }, this._node = _target, this._view = this._node.find(".view"), this._trigger = this._node.find(".btn_toggle"),
            this._trigger_pc = this._node.siblings('.default');
            this._inner = this._node.find(".accordion_inner"), this._trigger.bind("click", this.toggleHD), this._trigger_pc.bind('click', this.toggleHD),
            this._node.attr("data-default") && this.jadgeDefault();
            this._trigger_pc.bind('focus', this.show);
        }
        return __extends(Accordion, _super), Accordion;
    }(EventDispatcher);
    module.exports = Accordion;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), PullDown = __webpack_require__(12), PullDownMaker = function(_super) {
        function PullDownMaker() {
            _super.call(this), $(".pull_down").each(function() {
                new PullDown($(this));
            });
        }
        return __extends(PullDownMaker, _super), PullDownMaker;
    }(EventDispatcher);
    module.exports = PullDownMaker;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Ease = __webpack_require__(4), PullDown = function(_super) {
        function PullDown(_node) {
            var _this = this;
            _super.call(this), this._node = _node, this._isOpen = !1, this.toggleHD = function() {
                _this._isOpen = !_this._isOpen, _this._isOpen ? _this.show() : _this.hide();
            }, this.show = function() {
                _this._btnToggle.addClass("active"), _this._view.velocity("stop").velocity({
                    height: _this._inner.outerHeight()
                }, {
                    duration: 500,
                    delay: 0,
                    easing: Ease.EaseOutSine
                });
            }, this.hide = function() {
                _this._btnToggle.removeClass("active"), _this._view.velocity("stop").velocity({
                    height: 0
                }, {
                    duration: 500,
                    delay: 0,
                    easing: Ease.EaseOutSine
                });
            }, this._btnToggle = this._node.find(".btn_toggle"), this._view = this._node.find(".view"),
            this._inner = this._node.find(".pull_down_inner"), this._btnToggle.bind("click", this.toggleHD),
            this._view.css({
                height: 0
            });
            this._view.find('a').on('focus', function(){
                if(!_this._btnToggle.hasClass('avtive')){
                    _this.show();
                }
            });
        }
        return __extends(PullDown, _super), PullDown;
    }(EventDispatcher);
    module.exports = PullDown;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), TabFocus = function(_super) {
        function TabFocus() {
            _super.call(this), this.onClickHD = function(e) {
                13 === e.keyCode && ($(e.currentTarget).parent().trigger("click"), e.preventDefault());
            }, this.tapHD = function(e) {
                $(e.currentTarget).parent().trigger("click"), $(e.currentTarget).blur(), e.preventDefault();
            };
            var _self = this;
            $("a.ui").each(function() {
                $(this).keydown(_self.onClickHD), $(this).bind("click", _self.tapHD);
            });
        }
        return __extends(TabFocus, _super), TabFocus;
    }(EventDispatcher);
    module.exports = TabFocus;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), PageAnchor = function(_super) {
        function PageAnchor() {
            _super.call(this), $("a[href^=#]").click(function() {
                var href = $(this).attr("href"), target = $("#" == href || "" == href ? "htm" : href);
                return target.velocity("stop").velocity("scroll", {
                    duration: 600,
                    delay: 0,
                    easing: "easeInOutSine"
                }), !1;
            });
        }
        return __extends(PageAnchor, _super), PageAnchor;
    }(EventDispatcher);
    module.exports = PageAnchor;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Slider = __webpack_require__(16), SlideSetter = function(_super) {
        function SlideSetter() {
            _super.call(this), $(".Slider").each(function() {
                new Slider($(this));
            });
        }
        return __extends(SlideSetter, _super), SlideSetter;
    }(EventDispatcher);
    module.exports = SlideSetter;
}, function(module, exports, __webpack_require__) {
    var __extends = this && this.__extends || function(d, b) {
        function __() {
            this.constructor = d;
        }
        for (var p in b) b.hasOwnProperty(p) && (d[p] = b[p]);
        d.prototype = null === b ? Object.create(b) : (__.prototype = b.prototype, new __());
    }, EventDispatcher = __webpack_require__(1), Slider = function(_super) {
        function Slider(_slider) {
            var _this = this;
            _this.mode = 'sp';
            _super.call(this), this._slider = _slider, this.setup = function() {
                if (_this._slider.find("section").show(), _this._slider.css({
                    height: "auto"
                }), _this._stop = _this._slider.find(".sliderControlStop"), _this._inner = _this._slider.find(".SliderInner"),
                _this._btns = _this._slider.find(".sliderControlBtns"), _this._allCount = _this._slider.find(".allCount"),
                _this._allCount.text(_this._slider.find(".SliderInner > .slickSlide").length),
                _this._currentCount = _this._slider.find(".currentCount"),
                _this._inner.attr({'aria-valuemin': 1, 'aria-valuemax': _this._slider.find(".SliderInner > .slickSlide").length}),
                1 === _this._slider.find(".SliderInner > .slickSlide").length) return void _this._slider.find(".SliderControl").remove();
                _this._slider.find(".SliderInner > .slickSlide > a").each(function(index, element){
                    //PC画像を挿入
                    /*
                    var pc_image = $(element).children('img').addClass('sp-element').attr('src').replace(/\-sp\.(jpg|png)$/, '-pc.$1');
                    if(!pc_image.match(/\-pc\.(jpg|png)$/)){
                        pc_image = $(element).children('img').addClass('sp-element').attr('src').replace(/\.(jpg|png)$/, '-pc.$1');
                    }
                    $(element).append($(element).children('img').clone().addClass('pc-element').removeClass('sp-element').attr({src: pc_image}));
                    */
                });
                var _refStrNext = "次へ", _refStrPrev = "前へ", _refStrPlay = "再生", _refStrPause = "停止";
                switch (_this.language) {
                  case "en":
                    _refStrNext = "Next", _refStrPrev = "Previous", _refStrPlay = "Play", _refStrPause = "Stop";
                    break;
                  case "fr":
                    _refStrNext = "Suivant", _refStrPrev = "Précédent", _refStrPlay = "Marche", _refStrPause = "Arrêt";
                }
                var _self = _this;
                function _toggleStop (e) {
                    _self._stop.is(".active") ? (_self._stop.removeClass("active").find('a').attr({'aria-pressed': false}), _self._inner.slick("slickPlay").removeClass("active"),
                    $(".sliderControlStop").find("a")/*.text(_refStrPause)*/, e.preventDefault()) : (_self._stop.addClass("active").find('a').attr({'aria-pressed': true}),
                    _self._inner.slick("slickPause").addClass("active"), $(".sliderControlStop").find("a")/*.text(_refStrPlay)*/,
                    e.preventDefault());
                }
                _this._inner.slick({
                    centerMode: !0,
                    autoplay: !0,
                    pauseOnHover: !0,
                    centerPadding: "0px",
                    zIndex: 2,
                    autoplaySpeed: 3e3,
                    mobileFirst: !0,
                    infinite: !0,
                    appendArrows: _this._btns,
                    prevArrow: '<div class="btn sliderPrevArrow"><a class="ui" role="button" href="javascript:void(0);" aria-label="' + _refStrPrev + '"><img src="/assets/img/top/icon_arrow01_left.png" alt="' + _refStrPrev + '"></a></div>',
                    nextArrow: '<div class="btn sliderNextArrow"><a class="ui" role="button" href="javascript:void(0);" aria-label="' + _refStrNext + '"><img src="/assets/img/top/icon_arrow01_right.png" alt="' + _refStrNext + '"></a></div>'
                }).on("afterChange", function(event, slick, currentSlide) {
                    _self._currentCount.text(currentSlide + 1);
                    _this._inner.attr({'aria-valuenow': currentSlide + 1});
                    if(_self.mode === 'pc' && $(this).hasClass('pc-open')){
                        //PCでは全パネルにフォーカスを当てる
                        setTimeout(function(){
                            slick.$slides.attr({'tabindex': -1 ,'area-hidden': false}).removeAttr('tabindex').find('a').attr({'tabindex': 0});
                        },50);
                    }
                }), $(".sliderControlStop").find("a img").text(_refStrPause), $(".sliderControlStop").find("a img.active").text(_refStrPlay)/*.text(_refStrPause)*/,
                _self._stop.find('a').on('click', _toggleStop);
                _self._btns.find('a.ui').on('keydown', function(e){
                    if(e.keyCode === 13 || e.keyCode === 32){
                        // Enterキー、スペースキー
                        e.preventDefault();
                        if($(this).parent('div').hasClass('sliderControlStop')){
                            _toggleStop(e);
                        } else {
                            $(this).trigger('click');
                        }
                    }
                });
                $(window).on('focus', function(){
                    if(_self._stop.is(".active")){
                        _self._inner.slick("slickPause");
                    }
                });
                _this._inner.on('startPcMode', function(){
                    // assets/js/common/script.js
                    _self.mode = 'pc';
                    if($(this).hasClass('pc-open')){
                        _self._inner.find('.slick-track > .slick-slide').attr({'tabindex': -1 ,'aria-hidden': false}).removeAttr('tabindex').find('a').attr({'tabindex': 0});
                        _self._inner.find('.slick-track > .slick-slide.slick-cloned').attr({'tabindex': -1, 'aria-hidden': true}).find('a').attr({'tabindex': -1})
                    }
                }).on('endPcMode', function(){
                    _self.mode = 'sp';
                }).on('mouseleave', function(){
                    // マウスアウト時に自動スライドを改めてオフにする
                    if(_self._stop.is(".active")){
                        _self._inner.slick("slickPause");
                    }
                });
            }, this.language = document.body.getAttribute("id"), this.setup();
        }
        return __extends(Slider, _super), Slider;
    }(EventDispatcher);
    module.exports = Slider;
} ]);
