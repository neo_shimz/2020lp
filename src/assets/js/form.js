window.addEventListener('message', function(e) {
  var yobi = 0;
  if(e.data == 0){yobi = 5000;}
  if(e.origin=="https://krs.bz"){
    var custom_height;
    if(window.innerWidth > 768){
      // custom_height = 30;
      // custom_height = 130;
      custom_height = 280;
    }else if(window.innerWidth > 420) {
      // custom_height = 65;
      // custom_height = 165;
      custom_height = 280;
    }else{
      // custom_height = 65;
      // custom_height = 165;
      custom_height = 280;
    }
    document.getElementById('iframe_krs').height = e.data + yobi + custom_height;
  }
}, false);
var domStyleWatcher = {
  Start: function(tgt, styleobj){
    function eventHappen(data1, data2){
      var throwval = tgt.css(styleobj);
      tgt.trigger('domStyleChange', [throwval]);
    }
    var tge = tgt[0];
    var filter = ['height'];
    var options = {
      attributes: true,
      attributeFilter: filter
    };
    var mutOb = new MutationObserver(eventHappen);
    mutOb.observe(tge, options);
    return mutOb;
  },
  Stop: function(mo){
    mo.disconnect();
  }
};
var target = $('#iframe_krs');//ターゲット
function catchEvent(event, value){
  $( 'html,body', parent.document ).animate( { scrollTop: 0 }, 500 );
}
target.on('domStyleChange', catchEvent);
var dsw = domStyleWatcher.Start(target, 'height');

$(function() {
  $('#pageTopBox').hide();
  $('.pageTopFloat').on('click',function(){
    $( 'html,body', parent.document ).animate( { scrollTop: 0 }, 500 );
  });
  //スクロールが下まで届いたらフッターを避ける
  $(window).bind("scroll", function() {
    scrollHeight = $(document).height();
    scrollPosition = $(window).height() + $(window).scrollTop();
    if ( (scrollHeight - scrollPosition) / scrollHeight <= 0.02) {
      $('.pageTopFloat').css('bottom','150px');
    } else {
      $('.pageTopFloat').css('bottom','0px');
    }
  });
  //ブラウザの戻るボタンを無効にする
  history.pushState(null, null, null);
  $(window).on("popstate", function (event) {
    if (!event.originalEvent.state) {
      history.pushState(null, null, null);
      return;
    }
  });

});
