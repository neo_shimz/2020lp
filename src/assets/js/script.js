//javascript
//お手本動画を管理する
var data =[
	{"url":"//www.youtube.com/embed/JcvuoJgyIfk","thumbnail":"//img.youtube.com/vi/JcvuoJgyIfk/maxresdefault.jpg","japan":"バックストリート・ボーイズ","intro":"Backstreet Boys","style":""}, //バックストリート・ボーイズ
	{"url":"//www.youtube.com/embed/5RnnM5xfaPc","thumbnail":"//img.youtube.com/vi/5RnnM5xfaPc/maxresdefault.jpg","japan":"吉村崇さん","intro":"Takashi Yoshimura","style":""}, //吉村崇さん
	{"url":"//www.youtube.com/embed/l3U6gU6QLPw","thumbnail":"//img.youtube.com/vi/l3U6gU6QLPw/maxresdefault.jpg","japan":"ミキ","intro":"Miki","style":""}, //ミキ
	{"url":"//www.youtube.com/embed/Ocif9cSAm4Y","thumbnail":"//img.youtube.com/vi/Ocif9cSAm4Y/maxresdefault.jpg","japan":"仲里依紗さん","intro":"Riisa Naka","style":""}, //仲里依紗
	{"url":"//www.youtube.com/embed/5C195M3j2bQ","thumbnail":"//img.youtube.com/vi/5C195M3j2bQ/maxresdefault.jpg","japan":"木嶋真優さん","intro":"Mayu Kishima","style":""}, //木嶋真優
	{"url":"//www.youtube.com/embed/YyhP1v7Wu6Y","thumbnail":"//img.youtube.com/vi/YyhP1v7Wu6Y/maxresdefault.jpg","japan":"XTRAP","intro":"XTRAP","style":""}, //XTRAP
	{"url":"//www.youtube.com/embed/UmaGGfe6wY4","thumbnail":"//img.youtube.com/vi/UmaGGfe6wY4/maxresdefault.jpg","japan":"田村淳さん","intro":"Atsushi Tamura","style":""}, //田村淳
	{"url":"//www.youtube.com/embed/ujsblTzAPUM","thumbnail":"//img.youtube.com/vi/ujsblTzAPUM/maxresdefault.jpg","japan":"夢みるアドレセンス","intro":"YUMEMIRU ADOLESCENCE","style":""}, //夢みるアドレセンス
	{"url":"//www.youtube.com/embed/YQltsjO3kmc","thumbnail":"//img.youtube.com/vi/YQltsjO3kmc/maxresdefault.jpg","japan":"能楽堂","intro":"NOH THEATRE","style":"none"}, //能楽堂
	{"url":"//www.youtube.com/embed/OVQYjuK4U78","thumbnail":"//img.youtube.com/vi/OVQYjuK4U78/maxresdefault.jpg","japan":"オリンピックスタジアム","intro":"OYMPIC STADIUM","style":"none"}, //OYMPIC STADIUM
	{"url":"//www.youtube.com/embed/yLlZnsLQhSk","thumbnail":"//img.youtube.com/vi/yLlZnsLQhSk/maxresdefault.jpg","japan":"さんさ踊り","intro":"SANSA ODORI","style":"none"} //SANSA ODORI
//	{"url":"//www.youtube.com/embed/JCVLl7NQ_xs","thumbnail":"//img.youtube.com/vi/JCVLl7NQ_xs/maxresdefault.jpg","japan":"","intro":"","style":""}, //STEP DANCE
//	{"url":"//www.youtube.com/embed/1kKcy0iSmmQ","thumbnail":"//img.youtube.com/vi/1kKcy0iSmmQ/maxresdefault.jpg","japan":"","intro":"","style":""}, //SIGN LANGUAGE
//	{"url":"//www.youtube.com/embed/MrtLB0GVbkg","thumbnail":"//img.youtube.com/vi/MrtLB0GVbkg/maxresdefault.jpg","japan":"","intro":"","style":""}, //NIPPON BUDOKAN
//	{"url":"//www.youtube.com/embed/WtvRpqv4ioI","thumbnail":"//img.youtube.com/vi/WtvRpqv4ioI/maxresdefault.jpg","japan":"","intro":"","style":""} //TOKYO INTERNATIONAL FORUM
];
if($('body').attr('id')=='jp'){
	for(var i = 0;i<data.length;i++){
		$("#sample-move").append('<li><a href="'+data[i].url+'?rel=0" data-rel="lightcase"><img src="'+data[i].thumbnail+'" alt=""><span class="intro '+data[i].style+'">'+data[i].japan+'</span></a></li>');
	}
}else{
	for(var i = 0;i<data.length;i++){
		$("#sample-move").append('<li><a href="'+data[i].url+'?rel=0" data-rel="lightcase"><img src="'+data[i].thumbnail+'" alt=""><span class="intro '+data[i].style+'">'+data[i].intro+'</span></a></li>');
	}
}
//DOMができた後の処理
$(function() {
	//2番目まで「NEW」を付ける。（-n+●）●の部分を調整する（例：5なら-n+5）
	$("#sample-move").children("li:nth-child(-n+1)").addClass('new');
	//お手本動画の7番目以降をトグルで隠す
	$("#sample-move").children("li:nth-child(n+7)").wrapAll('<ul id="accordion-wrapper" class="sample-move"></ul>');
	$('#accordion-wrapper').ready(function() {
		$('#accordion-wrapper').hide();
		$('.accordion-open').on('click', function() {
			$('#accordion-wrapper').slideToggle(200);
			$('.accordion-close').css('display', 'block');
			$('.accordion-open').css('display', 'none');
		});
		$('.accordion-close').on('click', function() {
			$('#accordion-wrapper').slideToggle(200);
			$('.accordion-close').css('display', 'none');
			$('.accordion-open').css('display', 'block');
		});
	});
	//lightcase
	$('a[data-rel^=lightcase]').lightcase();
	//ページ内リンクを操作する
	$('.page_navi').on('click',function(){
		$(this).toggleClass('active');
		$(this).next().slideToggle();
	});
	$('.page_navi-a').on('click',function(){
		$('.page_navi-link').slideToggle(100);
		$('.page_navi').toggleClass('active');
	});
	//ページTOPへ戻る（テンプレートに存在するボタンは消す）
	$('#pageTopBox').hide();
	$('.pageTopFloat').on('click',function(){
		$( 'html,body', parent.document ).animate( { scrollTop: 0 }, 500 );
	});
	//スクロールが下まで届いたらフッターを避ける
	$(window).bind("scroll", function() {
		scrollHeight = $(document).height();
		scrollPosition = $(window).height() + $(window).scrollTop();
		if ( (scrollHeight - scrollPosition) / scrollHeight <= 0.02) {
			$('.pageTopFloat').css('bottom','150px');
		} else {
			$('.pageTopFloat').css('bottom','0px');
		}
	});
});
//ブラウザの戻るボタンを無効にする
// history.pushState(null, null, null);
// $(window).on("popstate", function (event) {
//   if (!event.originalEvent.state) {
//     history.pushState(null, null, null);
//     return;
//   }
// });
